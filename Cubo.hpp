#ifndef CUBO_HPP
#define CUBO_HPP

#include <SFML/Graphics.hpp>


class Cubo : public sf::Drawable, public sf::Transformable
{
public:
    Cubo(): Cubo(0){}
    Cubo(int value);
    /*Cubo(const Cubo& c):mRect(c.mRect), mText(c.mText), mValue(c.mValue){
        //mText.setFont(mFont);
        mIsVisible= c.mIsVisible;
        setPosition(c.getPosition());
    }
    Cubo& operator=(Cubo const& other){
        if(this!= &other){
            mRect= other.mRect;
            mText= other.mText;
            mValue= other.mValue;
            //mText.setFont(mFont);
            mIsVisible= other.mIsVisible;
            setPosition(other.getPosition());
        }
        return *this;
    }*/
    void show() { mIsVisible= true; }
    void hide() { mIsVisible= false; }
    sf::Vector2f getPosIterator() const;
    sf::Vector2f getPostSelect() const;
    void setValue(int value);
    bool operator<(const Cubo& other);
    bool operator==(const Cubo& other);
    bool operator!=(const Cubo& other);
protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states)const override;
private:
    sf::RectangleShape  mRect;
    sf::Text            mText;
    int                 mValue;
    bool                mIsVisible;
    static sf::Font     fuente;
    static bool         fuenteCargada;
};

#endif // CUBO_HPP
