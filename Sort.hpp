#ifndef SORT_HPP
#define SORT_HPP

#include<vector>
#include<functional>
#include<queue>
#include"Cubo.hpp"
#include"Functors.hpp"
#include<SFML/Graphics.hpp>

using Lista= std::vector<Cubo>;
using Cola= std::queue<std::function<bool(float)> >;
using Iterator= sf::RectangleShape;

class Sort: public sf::Drawable, public sf::Transformable
{
public:
    Sort();
    Sort(std::initializer_list<int> lista);
    //ordena el array por el método de la burbuja
    void buble_sort();
    //ordena el array por el método de seleccion
    void selection_sort();
    //Ordena el array por el metodo de insert sort
    void insert_sort();
    //ordena el array por el metodo quick sort
    void quick_sort();
    void quick_sort(Lista& lista, int inicio, int fin);
    //ordena el array por el metodo de merge sort
    void merge_sort();
    void merge_sort(Lista& lista, int inicio, int fin);
    bool run(float dt);
    void mezclar();
    /*void cambiarValores(){}
    void cambiarTamanio(int n){}*/
    void pause();
    void resume();
    bool isEnded() const;
protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states)const override;
private:
    Lista mLista;
    Cola  mCola;
    bool  mPaused;
    Iterator mUno, mDos, mTres;
    Cubo  mSelect;
};

#endif // SORT_HPP
