TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += libsfml-graphics libsfml-window libsfml-system

SOURCES += \
        Cubo.cpp \
        Functors.cpp \
        Sort.cpp \
        Menu.cpp \
        main.cpp

HEADERS += \
    Cubo.hpp \
    Functors.hpp \
    Sort.hpp \
    Menu.hpp
