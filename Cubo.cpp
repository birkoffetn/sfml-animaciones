#include "Cubo.hpp"
#include <iostream>

//definicion de variables estaticas de la clase
bool Cubo::fuenteCargada= false;

sf::Font Cubo::fuente= sf::Font();

//Definicion de la intefaz de la clase
Cubo::Cubo(int value){
    mRect.setSize({50, 50});
    //mRect.setOrigin(25, 25);
    mRect.setFillColor(sf::Color::Blue);
    if(!fuenteCargada){
        fuenteCargada= fuente.loadFromFile("fonts/font.ttf");
        if(fuenteCargada){
            std::cout<<"Todo ok..."<<std::endl;
        }else{
            std::cout<<"La fuente no ha sido cargada"<<std::endl;
        }
    }
    mText.setFont(fuente);
    mText.setCharacterSize(30);
    mText.setFillColor(sf::Color::White);
    setValue(value);
    mIsVisible= true;
}

sf::Vector2f Cubo::getPosIterator() const{
    sf::Vector2f pos= getPosition();
    pos.y+= 60;
    return pos;
}

sf::Vector2f Cubo::getPostSelect() const
{
    sf::Vector2f pos= getPosition();
    pos.y-= 60;
    return pos;
}

void Cubo::setValue(int value){
    mValue= value;
    mText.setString(std::to_string(mValue));
}

bool Cubo::operator<(const Cubo &other){
    return mValue< other.mValue;
}

bool Cubo::operator==(const Cubo &other){
    return mValue== other.mValue;
}

bool Cubo::operator!=(const Cubo &other){
    return mValue!= other.mValue;
}

void Cubo::draw(sf::RenderTarget &target, sf::RenderStates states) const{
    if(mIsVisible== true){
        states.transform*= getTransform();
        target.draw(mRect, states);
        target.draw(mText, states);
    }
}
