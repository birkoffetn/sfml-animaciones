#ifndef FUNCTORS_HPP
#define FUNCTORS_HPP

#include<SFML/Graphics.hpp>
#include<iostream>
#include"Cubo.hpp"
#include<vector>

#define TIME_SWAP 1.0f


class Hide
{
public:
    Hide(Cubo& c): cubo(c){}
    bool operator()(float){
        cubo.hide();
        return false;
    }
private:
    Cubo& cubo;
};

class Show
{
public:
    Show(Cubo& c): cubo(c){}
    bool operator()(float){
        cubo.show();
        return false;
    }
private:
    Cubo& cubo;
};

class Select{
    Cubo &mOrigen, &mDestino;
public:
    Select(Cubo& origen, Cubo& destino): mOrigen(origen), mDestino(destino){}
    bool operator()(float){
        //auto pos= mOrigen.getPosition();
        mDestino= mOrigen;
        mDestino.setPosition(mOrigen.getPostSelect());
        mDestino.show();
        return false;
    }
};

class Swap{
public:
    Swap(Cubo& a, Cubo& b): ma(a), mb(b), mpa(ma.getPosition()), mpb(mb.getPosition()){
        mVel= mpb- mpa;
        mVel/= TIME_SWAP;
        mEnded= false;
        mElapsed= 0.0f;
    }
    bool operator()(float dTime){
        if(mEnded) return false;
        if(mElapsed< TIME_SWAP){
            mElapsed+= dTime;
            ma.move(dTime*mVel);
            mb.move(-dTime*mVel);
            return true;
        }else{
            std::swap(ma, mb);
            ma.setPosition(mpa);
            mb.setPosition(mpb);
            mEnded= true;
            return false;
        }
    }
private:
    Cubo              &ma, &mb;
    sf::Vector2f      mpa, mpb;
    sf::Vector2f      mVel;
    float             mElapsed;
    bool              mEnded;
};

class MoveTo{
public:
    MoveTo(sf::Transformable& obj, sf::Vector2f const& inicio, sf::Vector2f const& final, float time= 0.5f):
        mObj(obj), mPosInicio(inicio), mPosFin(final)
    {
        mElapsed= 0.0f;
        mTimeMov= time;
        mVel= mPosFin- mPosInicio;
        mVel/= mTimeMov;
    }
    bool operator()(float dTime){
        mElapsed+= dTime;
        if(mElapsed< mTimeMov){
            mObj.move(dTime*mVel);
            return true;
        }
        else{
            mObj.setPosition(mPosFin);
            return false;
        }
    }
private:
    sf::Transformable&  mObj;
    sf::Vector2f        mPosInicio, mPosFin, mVel;
    float               mElapsed;
    float               mTimeMov;
};

class Timer{
public:
    Timer(sf::Transformable& t, sf::Vector2f const& pos, float time= 0.5f): mT(t), mPos(pos), mTime(time), mElapsed(0.0f){

    }
    bool operator()(float dt){
        if(mElapsed< mTime){
            mT.setPosition(mPos);
            mElapsed+= dt;
            return true;
        }
        else return false;
    }
private:
    sf::Transformable&  mT;
    sf::Vector2f        mPos;
    float mTime,        mElapsed;
};

class Copy{
    Cubo mOrigen, &mDestino;
    float mElapsed, mTime;
public:
    Copy(Cubo origen, Cubo& destino, float time= 0.0): mOrigen(origen), mDestino(destino), mElapsed(0.0f), mTime(time){}
    bool operator()(float dt){
        if(mElapsed< mTime){
            mElapsed+= dt;
            return true;
        }else{
            mDestino= mOrigen;
            return false;
        }
    }
};

class ShowAll{
    std::vector<Cubo>& mLista;
public:
    ShowAll(std::vector<Cubo>& lista): mLista(lista) {}
    bool operator()(float){
        for(auto& cubo: mLista) cubo.show();
        return false;
    }
};


#endif // FUNCTORS_HPP
