#include "Sort.hpp"
#include <iostream>
#include <algorithm>
#include <random>
#include <ctime>

Sort::Sort()
{

}

Sort::Sort(std::initializer_list<int> lista){
    int i= 0;
    for(auto const& v: lista){
        Cubo temp(v);
        temp.setPosition(i, 0);
        std::cout<<"Cubo "<<i<<": "<<temp.getPosition().x<<", "<<temp.getPosition().y<<std::endl;
        mLista.push_back(v);
        mLista[i].setPosition(i* 60, 0);
        ++i;
    }
    /*i= 0;
    for(auto& c: mLista){
        c.setPosition(i, 0);
        std::cout<<"Cubo "<<i<<": "<<c.getPosition().x<<", "<<c.getPosition().y<<std::endl;
        i+= 60;
    }*/
    mPaused= true;
    mUno.setSize({20, 20});
    mDos.setSize({20, 20});
    mTres.setSize({20, 20});

    mUno.setFillColor(sf::Color::Red);
    mDos.setFillColor(sf::Color::White);
    mTres.setFillColor(sf::Color::White);

    mUno.setPosition(-1000, -1000);
    mDos.setPosition(-1000, -1000);
    mTres.setPosition(-1000, -1000);
    mSelect.hide();
}

void Sort::buble_sort(){
    Lista temp= mLista;
    int i= 0, j= i+ 1, c= mLista.size(), d= 0;
    --c;
    if(i< c){
        mCola.push(Timer(mUno, mLista[0].getPosIterator()));
        mCola.push(Timer(mDos, mLista[1].getPosIterator()));
    }else{
        return;
    }
    while(i< c){
        while(i< c){
            j= i+ 1;
            if(temp[j]< temp[i]){
                Cubo tmp= temp[i];
                temp[i]= temp[j];
                temp[j]= tmp;
                mCola.push(Swap(mLista[i], mLista[j]));
                d= i;
            }
            ++i;
            ++j;
            if(i< c){
                mCola.push(MoveTo(mUno, mLista[i- 1].getPosIterator(), mLista[j-1].getPosIterator(), 0.25));
                mCola.push(MoveTo(mDos, mLista[i].getPosIterator(), mLista[j].getPosIterator(), 0.25));
            }else{
                mCola.push(Timer(mUno, mLista[0].getPosIterator(), 0.25f));
                mCola.push(Timer(mDos, mLista[1].getPosIterator(), 0.25f));
            }
        }
        i= 0;
        --c;
        if((d-1) < c) c= d;
        d= 0;
    }
    mCola.push(Timer(mUno, {-1000, -1000}));
    mCola.push(Timer(mDos, {-1000, -1000}));
}

void Sort::selection_sort(){
    if(mLista.size()< 2) return;
    Lista temp= mLista;
    int select;
    mUno.setPosition(mLista[0].getPosIterator());
    mDos.setPosition(mLista[0].getPosIterator());
    mTres.setPosition(mLista[0].getPosIterator());
    for(int i= 0; i< (int)temp.size()- 1; ++i){
//        sf::Vector2f inicio=  mLista[i].getPosIterator();
        sf::Vector2f fin=  mLista[i].getPosIterator();
        //mCola.push(MoveTo(mUno, mUno.getPosition(), fin));
        //mUno.setPosition(fin);
        
        mCola.push(MoveTo(mDos, mDos.getPosition(), fin));
        mDos.setPosition(fin);

        mCola.push(MoveTo(mTres, mTres.getPosition(), fin));
        mTres.setPosition(fin);

        select= i;

        for(int j= i+ 1; j< (int)temp.size(); ++j){
            sf::Vector2f inicio=  mUno.getPosition();
            sf::Vector2f fin=  mLista[j].getPosIterator();
            mCola.push(MoveTo(mUno, inicio, fin));
            mUno.setPosition(fin);

            if(temp[j]< temp[select]){
                sf::Vector2f inicio=  mDos.getPosition();
                sf::Vector2f fin=  mLista[j].getPosIterator();
                mCola.push(MoveTo(mDos, inicio, fin));
                mDos.setPosition(fin);
                select= j;
            }
        }
        if(select!= i){
            Cubo t= temp[select];
            temp[select]= temp[i];
            temp[i]= t;
            mCola.push(Swap(mLista[select], mLista[i]));
        }
    }
    mCola.push(Timer(mUno, {-1000, -1000}, 0.1));
    mCola.push(Timer(mDos, {-1000, -1000}, 0.1));
    mCola.push(Timer(mTres, {-1000, -1000}, 0.1));
    mUno.setPosition(-1000, -1000);
    mDos.setPosition(-1000, -1000);
    mTres.setPosition(-1000, -1000);
}

void Sort::insert_sort(){
    if(mLista.size()< 2) return;
    Lista temp= mLista;
    mUno.setPosition(mLista[0].getPosIterator());
    mDos.setPosition(mLista[1].getPosIterator());
    for(int i= 1; i< (int)mLista.size(); ++i){
        auto fin= mLista[i].getPosIterator();
        mCola.push(MoveTo(mDos, mDos.getPosition(), fin));
        mDos.setPosition(fin);
        for(int j= i- 1; -1< j; --j){
            auto fin= mLista[j].getPosIterator();
            mCola.push(MoveTo(mUno, mUno.getPosition(), fin));
            mUno.setPosition(fin);
            if(temp[j+1]< temp[j]){
                Cubo t= temp[j+1];
                temp[j+1]= temp[j];
                temp[j]= t;
                mCola.push(Swap(mLista[j+1], mLista[j]));
            }else{
                break;
            }
        }
    }
    mCola.push(Timer(mUno, {-1000, -1000}, 0.1));
    mCola.push(Timer(mDos, {-1000, -1000}, 0.1));
    mUno.setPosition(-1000, -1000);
    mDos.setPosition(-1000, -1000);
}

void Sort::quick_sort(){
    if(mLista.size()< 2) return;
    Lista temp= mLista;

    auto pos1= mLista[0].getPosIterator();
    mUno.setPosition(pos1);
    auto pos2= mLista[mLista.size()- 1].getPosIterator();
    mDos.setPosition(pos2);
    mCola.push(MoveTo(mUno, pos1, pos1, 0.0));
    mCola.push(MoveTo(mDos, pos2, pos2, 0.0));

    quick_sort(temp, 0, temp.size()- 1);

    mCola.push(Timer(mUno, {-1000, -1000}, 0.1));
    mCola.push(Timer(mDos, {-1000, -1000}, 0.1));
    mCola.push(Hide(mSelect));
    mUno.setPosition(-1000, -1000);
    mDos.setPosition(-1000, -1000);
}
void Sort::quick_sort(Lista& lista, int inicio, int fin){
    if(!(inicio< fin)) return;

    auto pos1= mLista[inicio].getPosIterator();
    mCola.push(MoveTo(mUno, mUno.getPosition(), pos1, 0.25));
    mUno.setPosition(pos1);

    auto pivote= lista[inicio];
    mCola.push(Select(mLista[inicio], mSelect));
    mCola.push(Hide(mLista[inicio]));
    //mCola.push(Show(mSelect));

    auto pos2= mLista[fin].getPosIterator();
    mCola.push(MoveTo(mDos, mDos.getPosition(), pos2, 0.25));
    mDos.setPosition(pos2);
    //mSelect.setPosition(pos1.x, pos1.x -100);
    //mCola.push(Swap(mLista[inicio], mSelect));
    /*lista[m]= lista[inicio];
    mCola.push(Swap(mLista[m], mLista[inicio]));*/
    int i= inicio, j= fin;
    
    while(i< j){
        while(i< j){
            if(pivote< lista[j]){
                --j;
                auto pos= mLista[j].getPosIterator();
                mCola.push(MoveTo(mDos, mDos.getPosition(), pos, 0.5));
                mDos.setPosition(pos);
            }
            else{
                lista[i]= lista[j];
                mCola.push(Swap(mLista[i], mLista[j]));
                ++i;
                auto pos= mLista[i].getPosIterator();
                mCola.push(MoveTo(mUno, mUno.getPosition(), pos, 0.5));
                mUno.setPosition(pos);
                break;
            }
        }
        while(i< j){
            if(lista[i]< pivote){
                ++i;
                auto pos= mLista[i].getPosIterator();
                mCola.push(MoveTo(mUno, mUno.getPosition(), pos, 0.5));
                mUno.setPosition(pos);
            }
            else{
                lista[j]= lista[i];
                mCola.push(Swap(mLista[i], mLista[j]));
                --j;
                auto pos= mLista[j].getPosIterator();
                mCola.push(MoveTo(mDos, mDos.getPosition(), pos, 0.5));
                mDos.setPosition(pos);
                break;
            }
        }
    }
    lista[i]= pivote;
    mCola.push(Show(mLista[i]));
    mCola.push(Hide(mSelect));
    if(i> inicio) quick_sort(lista, inicio, i- 1);
    if(i< fin)    quick_sort(lista, i+ 1, fin);
}

void Sort::merge_sort()
{
    if(mLista.size()< 2) return;
    Lista temp= mLista;
    //Lista merge= mLista;

    mCola.push(Hide(mSelect));

    auto pos1= mLista[0].getPosIterator();
    mUno.setPosition(pos1);
    mCola.push(Timer(mUno, pos1, 0.05));
    auto pos2= mLista[mLista.size()- 1].getPosIterator();
    mDos.setPosition(pos2);
    mCola.push(Timer(mDos, pos2, 0.05));

    merge_sort(temp, 0, temp.size());

    mDos.setPosition({-1000, -1000});
    mCola.push(Timer(mDos, {-1000, -1000}, 0.1));
    mUno.setPosition({-1000, -1000});
    mCola.push(Timer(mUno, {-1000, -1000}, 0.1));
    mCola.push(ShowAll(mLista));
}

void Sort::merge_sort(Lista& lista, int inicio, int n)
{
    if(n< 2){
        if(n== 1){
            lista.front()= mLista[inicio];
        }
        return;
    }
    int nIzq= n/2;
    int nDer= n- nIzq;
    Lista izq(nIzq);
    Lista der(nDer);
    merge_sort(izq, inicio, nIzq);
    merge_sort(der, inicio+ nIzq, nDer);

    //auto pos1= mLista[inicio].getPosIterator();
    //mCola.push(MoveTo(mUno, mUno.getPosition(), pos1, 0.5));
    //mUno.setPosition(pos1);

    auto pos2= mLista[inicio+ n- 1].getPosIterator();
    mCola.push(MoveTo(mDos, mDos.getPosition(), pos2, 0.1));
    mDos.setPosition(pos2);

    auto ii= 0, id= 0, ll= 0;
    for(auto& l: lista){
        if(ii< nIzq){
            if(id< nDer){
                if(der[id]< izq[ii]){
                    l= der[id];
                    ++id;
                }
                else{
                    l= izq[ii];
                    ++ii;
                }
            }else{
                l= izq[ii];
                ++ii;
            }
        }
        else{
            l= der[id];
            ++id;
        }
        auto pos= mLista[ll+ inicio].getPosIterator();
        mCola.push(MoveTo(mUno, mUno.getPosition(), pos, 0.3));
        mUno.setPosition(pos);
        l.setPosition(mLista[ll+ inicio].getPosition());
        mCola.push(Copy(l, mLista[ll+ inicio], 0.1));
        mCola.push(Show(mLista[ll+ inicio]));
        ++ll;
    }
}

bool Sort::run(float dt){
    if(isEnded()) return false;
    if(mPaused) return true;
    if(mCola.front()(dt)){
        //std::cout<<"Cola ok..."<<std::endl;
    }else{
        mCola.pop();
    }
    return true;
}

void Sort::mezclar(){
    std::shuffle(mLista.begin(), mLista.end(), std::default_random_engine(std::time(nullptr)));
    int i= 0;
    for(auto &c: mLista){
        c.setPosition(i, 0);
        i+= 60;
    }
    while(!mCola.empty()) mCola.pop();
    mPaused= true;
}

void Sort::pause(){ mPaused= true; }

void Sort::resume(){ mPaused= false; }

bool Sort::isEnded() const { return mCola.empty(); }

void Sort::draw(sf::RenderTarget &target, sf::RenderStates states) const{
    states.transform*= getTransform();
    for(auto const& c: mLista){
        target.draw(c, states);
    }
    target.draw(mDos, states);
    target.draw(mUno, states);
    target.draw(mTres, states);
    target.draw(mSelect, states);
}
