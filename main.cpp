#include <iostream>
#include <SFML/Graphics.hpp>

#include"Cubo.hpp"
#include"Functors.hpp"
#include "Sort.hpp"
#include "Menu.hpp"
#include<fstream>
#include<vector>
#include<functional>

using namespace std;

void ordenar_vector(vector<Cubo>& vec, vector<function<bool(float)> >& cola, sf::Transformable& uno, sf::Transformable& dos){
    vector<Cubo>::iterator centinela, i, j;
    if(vec.size()< 2) return;
    centinela= vec.end();
    --centinela;

    while(i!= centinela){
        i= vec.begin();
        j= i+ 1;

        while(i!= centinela){
            if(i== vec.begin()){
                cout<<"Entre una vez aqui inicio ====>>>>>"<<endl;
                uno.setPosition(i->getPosIterator());
                dos.setPosition(j->getPosIterator());
            }else{
                cout<<"Estoy aqui agregando"<<endl;
                cola.push_back(MoveTo(uno, uno.getPosition(), i->getPosIterator()));
                cola.push_back(MoveTo(dos, dos.getPosition(), j->getPosIterator()));
            }
            if(*j< *i) cola.push_back(Swap(*i, *j));
            ++i;
            ++j;
            --centinela;
        }
    }
    uno.setPosition(-100, -100);
    dos.setPosition(-100, -100);
}


int main()
{
    sf::RenderWindow window;
    window.create(sf::VideoMode(800, 600), "Nueva Ventana");
    window.setFramerateLimit(60);
    //window.setVerticalSyncEnabled(true);

    Menu menu;
    std::string mensaje= "Menu uno";
    menu.addItem("Nuevo", [mensaje](){ std::cout<<"Se ha disparado una accion: "<<mensaje<<endl; });
    mensaje= "Menu dos";
    menu.addItem("Otro", [mensaje](){ std::cout<<"Se ha disparado una accion: "<<mensaje<<endl; });
    mensaje= "Menu tres";
    menu.addItem("De nuevo", [mensaje](){ std::cout<<"Se ha disparado una accion: "<<mensaje<<endl; });
    menu.setPosition(80, 80);

    Sort ordena{1, 2, 3, 4, 6, 5, 7, 8, 9, 10, 11, 12};
    ordena.setPosition(50, 500);
    ordena.mezclar();
    ordena.setOrigin(350, 40);
    ordena.setPosition(400, 400);
    //ordena.selection_sort();
    //ordena.buble_sort();
    //ordena.insert_sort();
    ordena.quick_sort();
    //ordena.merge_sort();
    ordena.resume();
    Cubo treinta(30);
    treinta.setPosition(50, 100);

    Cubo cincuenta(50);
    cincuenta.setPosition(400, 100);

    Swap cambio(treinta, cincuenta);

    sf::RectangleShape iUno, iDos;
    iUno.setSize({50, 50});
    iDos.setSize({50, 50});

    vector<Cubo> lista;
    lista.emplace_back(2);
    lista.emplace_back(5);
    lista.emplace_back(3);
    lista.emplace_back(1);
    lista.emplace_back(7);

    vector<function<bool(float)> > cola;

    /*ordenar_vector(lista, cola, iUno, iDos);
    cout<<"Elementos del vector cola: "<<cola.size()<<endl;

    auto paso= cola.begin();*/
    int i= 50.0;
    for(auto& l: lista){
        l.setPosition(i, 200);
        i+= 60;
    }

    Swap cambiodos(lista[0], lista[1]);

    while(window.isOpen()){
        sf::Event event;
        while(window.pollEvent(event)){
            if(event.type== sf::Event::Closed) window.close();
            menu.readEvent(event);
        }

        window.clear();

        /*if(paso!= cola.end()){
            cout<<"Entre aqui"<<endl;
            auto& func= *paso;
            if(func(0.02)){

            }
            else{
                cout<<"avanzando un pas"<<endl;
                paso++;
            }
        }*/
        cambio(0.01);
        cambiodos(0.005);
        ordena.run(0.02);
        for(auto const& l: lista) window.draw(l);
        window.draw(iUno);
        window.draw(iDos);
        window.draw(treinta);
        window.draw(cincuenta);
        //ordena.rotate(0.05);
        window.draw(ordena);
        window.draw(menu);

        window.display();
    }
    cout << "Hello World!" << endl;
    return 0;
}
