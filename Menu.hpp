#ifndef __menu__hpp__
#define __menu__hpp__

#include<vector>
#include<functional>
#include<SFML/Graphics.hpp>

using MenuItem = sf::Text;
using Action= std::function<void()>;
using Items = std::vector<std::pair<MenuItem, Action> >;

class Menu: public sf::Drawable, public sf::Transformable{
    Items mItems;
    int   mSelect;
    static sf::Font fuente;
    static bool estaCargada;

    int getItemOnMouseOver(int x, int y) const;
    void selectItemOnMouseOver(int x, int y);
    void selectNextItem();
    void selectPreviusItem();
    void execute();
public:
    Menu(): mSelect(0){}
    Menu(std::initializer_list<std::pair<sf::String, Action> > const&lista);
    void addItem(sf::String const& title, Action action);
    void readEvent(sf::Event const& event);
protected:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

#endif
