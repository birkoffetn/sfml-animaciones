#include"Menu.hpp"
#include<iostream>

sf::Font Menu::fuente= sf::Font();
bool     Menu::estaCargada= false;

Menu::Menu(std::initializer_list<std::pair<sf::String, Action> > const& lista){
    for(auto const& l: lista) addItem(l.first, l.second);
    int i= 0;
    for(auto& item: mItems){
        item.first.setPosition(i, 0);
        i+= 100;
    }
    mSelect= 0;
}

void Menu::addItem(sf::String const& title, Action action){
    MenuItem temp;
    if(!estaCargada){
        estaCargada= fuente.loadFromFile("fonts/font.ttf");
        if(estaCargada);
        else{
            exit(1);
        }
    }
    temp.setString(title);
    temp.setFont(fuente);
    temp.setCharacterSize(25);
    temp.setFillColor(sf::Color::White);
    mItems.push_back({temp, action});
    mItems.back().first.setPosition((mItems.size()- 1)*200, 0);
}

void Menu::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    states.transform*= getTransform();
    for(auto const& item: mItems){
        target.draw(item.first, states);
    }
}

void Menu::readEvent(sf::Event const& event){
    if(event.type== sf::Event::Closed){//en este punto se debe salir de la aplicacion
        //
    }
    if(event.type== sf::Event::KeyPressed){
        switch(event.key.code){
            case sf::Keyboard::Enter:
                //std::cout<<"Se ha presionado enter: "<<sf::Keyboard::Enter<<std::endl;
                mItems[mSelect].second();
                break;
            case sf::Keyboard::Tab:
                if(event.key.shift) selectPreviusItem();
                else selectNextItem();
                break;
            case sf::Keyboard::Space:
                //pausar
                //continuar de acuerdo al estado
                break;
            default: break;
        }
        //return;
    }
    if(event.type== sf::Event::MouseButtonPressed){
        auto mouse= event.mouseButton;
        if(mouse.button== 0 && getItemOnMouseOver(mouse.x, mouse.y)== mSelect){
            mItems[mSelect].second();
        }
        //return;
    }
    if(event.type== sf::Event::MouseMoved){
        selectItemOnMouseOver(event.mouseMove.x, event.mouseMove.y);
        //return;
    }
}

int Menu::getItemOnMouseOver(int x, int y) const {
    int i= 0;
    for(auto const& item: mItems){
        auto b= item.first.getGlobalBounds();
        b= getTransform().transformRect(b);
        if(b.contains(x, y)){
            std::cout<<"Left: "<<b.left<<" top: "<<b.top<<" with: "<<b.width<<" height: "<<b.height<<std::endl;
            std::cout<<"x= "<<x<<" y= "<<y<<" i= "<<i<<std::endl;
            return i;
        }
        ++i;
    }
    return -1;
}

void Menu::selectItemOnMouseOver(int x, int y){
    auto i= getItemOnMouseOver(x, y);
    if(i!= -1){
        mItems[mSelect].first.setFillColor(sf::Color::White);
        mItems[i].first.setFillColor(sf::Color::Red);
        mSelect= i;
    }
}

void Menu::selectNextItem(){
    mItems[mSelect].first.setFillColor(sf::Color::White);
    mSelect+= 1;
    mSelect%= mItems.size();
    mItems[mSelect].first.setFillColor(sf::Color::Red);
}

void Menu::selectPreviusItem(){
    mItems[mSelect].first.setFillColor(sf::Color::White);
    mSelect-= 1;
    mSelect+= mItems.size();
    mSelect%= mItems.size();
    mItems[mSelect].first.setFillColor(sf::Color::Red);
}

void Menu::execute(){

}
