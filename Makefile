OBJECTS= $(patsubst %.cpp,obj/%.o,$(wildcard *.cpp))


all: bin/main

bin/main: $(OBJECTS)
	g++ $^ -o $@ -lsfml-window -lsfml-system -lsfml-graphics

obj/%.o: %.cpp
	g++ -c $< -o $@

clean:
	rm -f $(OBJECTS) bin/main
